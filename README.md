# Toucan-Openapi

## Tech stack:
    
* Spring Boot
* Spring Data
* Spring Security
* Spring WebFlux
* OpenAPI
* Aspect

## Showcase

* OpenAPI yaml contract.
* Rest API interface generated from contract.
* Client generated from contract.
* Basic authentication.
* Logging methods with Aspect.
* Reactive API (basic)
* H2
* QueryDSL