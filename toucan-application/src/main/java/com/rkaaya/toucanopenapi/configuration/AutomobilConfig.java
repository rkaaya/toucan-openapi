package com.rkaaya.toucanopenapi.configuration;

import com.rkaaya.toucanopenapi.api.services.AutomobilService;
import com.rkaaya.toucanopenapi.services.DefaultAutomobilService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutomobilConfig {

    @Bean
    public AutomobilService automobilService() {
        return new DefaultAutomobilService();
    }
}
