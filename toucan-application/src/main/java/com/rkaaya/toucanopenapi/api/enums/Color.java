package com.rkaaya.toucanopenapi.api.enums;

public enum Color {
    GREEN, RED, BLACK;
}
