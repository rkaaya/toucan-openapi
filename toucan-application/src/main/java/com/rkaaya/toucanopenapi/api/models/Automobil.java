package com.rkaaya.toucanopenapi.api.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Automobil {
    private Integer id;
    private String name;
    private BigDecimal price;
    private Long model;
}
