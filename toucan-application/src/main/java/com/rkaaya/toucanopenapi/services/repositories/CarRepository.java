package com.rkaaya.toucanopenapi.services.repositories;

import com.rkaaya.toucanopenapi.services.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
}
