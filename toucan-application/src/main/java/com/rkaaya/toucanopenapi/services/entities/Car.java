package com.rkaaya.toucanopenapi.services.entities;

import com.rkaaya.toucanopenapi.api.enums.Color;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Car")
public class Car {

    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private Integer age;

    @Column
    private Color color;
}
