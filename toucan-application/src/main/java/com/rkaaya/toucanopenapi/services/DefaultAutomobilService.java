package com.rkaaya.toucanopenapi.services;

import com.rkaaya.toucanopenapi.api.services.AutomobilService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultAutomobilService implements AutomobilService {
    @Override
    public void automobilReport() {
        log.info("Report.");
    }
}
