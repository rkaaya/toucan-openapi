package com.rkaaya.toucanopenapi.infrastructure.rest;

import com.rkaaya.toucanopenapi.api.services.AutomobilService;
import com.rkaaya.toucanopenapi.server.api.SecureApi;
import com.rkaaya.toucanopenapi.server.model.Car;
import com.rkaaya.toucanopenapi.server.model.Cars;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
@Slf4j
public class SecureController implements SecureApi {

    private final AutomobilService automobilService;

    public Mono<ResponseEntity<Cars>> getSecureToucanCars(final ServerWebExchange exchange) {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        log.info("Logged in user: {}", auth.getName());
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok().build());
    }

    public Mono<ResponseEntity<Void>> addSecureCar(final Mono<Car> car, final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok().build());
    }
}
