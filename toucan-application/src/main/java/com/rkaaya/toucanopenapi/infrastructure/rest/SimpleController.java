package com.rkaaya.toucanopenapi.infrastructure.rest;

import com.rkaaya.toucanopenapi.api.services.AutomobilService;
import com.rkaaya.toucanopenapi.server.api.SimpleApi;
import com.rkaaya.toucanopenapi.server.model.Car;
import com.rkaaya.toucanopenapi.server.model.Cars;
import com.rkaaya.toucanopenapi.tools.LoggableAfterMethod;
import com.rkaaya.toucanopenapi.tools.LoggableBeforeMethod;
import com.rkaaya.toucanopenapi.tools.LoggableMeasureMethod;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
@Slf4j
public class SimpleController implements SimpleApi {

    private final AutomobilService automobilService;

    @LoggableBeforeMethod
    public Mono<ResponseEntity<Cars>> getToucanCars(final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok(null));
    }

    @LoggableBeforeMethod
    public Mono<ResponseEntity<Void>> addCar(final Mono<Car> car, final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok().build());
    }

    @LoggableBeforeMethod
    public Mono<ResponseEntity<Void>> deleteCar(final Long carId, final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok().build());
    }

    @LoggableMeasureMethod
    public Mono<ResponseEntity<Car>> getToucanCar(final Long carId, final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok(null));
    }

    @LoggableAfterMethod
    public Mono<ResponseEntity<Void>> updateCar(final Long carId, final Mono<Car> car, final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok(null));
    }
}
