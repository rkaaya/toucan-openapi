package com.rkaaya.toucanopenapi.infrastructure.rest;

import com.rkaaya.toucanopenapi.api.services.AutomobilService;
import com.rkaaya.toucanopenapi.server.api.TypesApi;
import com.rkaaya.toucanopenapi.server.model.CarInformation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@AllArgsConstructor
@Slf4j
public class TypesController implements TypesApi {

    private final AutomobilService automobilService;

    public Mono<ResponseEntity<Map<String, Integer>>> getToucanCarsInformation(final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok().build());
    }

    public Mono<ResponseEntity<Map<String, CarInformation>>> getToucanCarsInformationDict(final ServerWebExchange exchange) {
        automobilService.automobilReport();
        return Mono.just(ResponseEntity.ok().build());
    }
}
