package com.rkaaya.toucanopenapi.runnable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

@EntityScan("com.rkaaya.toucanopenapi.services.entities")
@EnableJpaRepositories(basePackages = {"com.rkaaya.toucanopenapi.services.repositories"})
@SpringBootApplication(scanBasePackages = {"com.rkaaya.toucanopenapi.configuration", "com.rkaaya.toucanopenapi.infrastructure.rest"})
@EnableWebFlux
public class ToucanOpenapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToucanOpenapiApplication.class, args);
    }
}
