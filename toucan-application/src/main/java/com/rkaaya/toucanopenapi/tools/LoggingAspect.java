package com.rkaaya.toucanopenapi.tools;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.Arrays;
import java.util.Collection;

@Aspect
@Slf4j
public class LoggingAspect {

    @Pointcut("@annotation(LoggableBeforeMethod)")
    public void executePreLogging() {

    }

    @Pointcut("@annotation(LoggableAfterMethod)")
    public void executeAfterLogging() {

    }

    @Pointcut("@annotation(LoggableMeasureMethod)")
    public void executeMeasureLogging() {

    }

    @Before("executePreLogging()")
    public void preLogMethodCall(JoinPoint joinPoint) {
        StringBuilder sb = new StringBuilder("Method: ");
        sb.append(joinPoint.getSignature().getName());
        Object[] args = joinPoint.getArgs();
        if (null != args && args.length > 0) {
            sb.append(" args= [ | ");
                Arrays.asList(args).forEach(arg -> {sb.append(arg).append(" | ");
            });
                sb.append("]");
        }
        log.info(sb.toString());
    }

    @AfterReturning(value = "executeAfterLogging()", returning = "returnValue")
    public void afterLogMethodCall(JoinPoint joinPoint, Object returnValue) {
        StringBuilder sb = new StringBuilder("Method: ");
        sb.append(joinPoint.getSignature().getName());
        Object[] args = joinPoint.getArgs();
        if (null != args && args.length > 0) {
            sb.append(" args= [ | ");
            Arrays.asList(args).forEach(arg -> {sb.append(arg).append(" | ");
            });
            sb.append("]");
        }

        sb.append(", returns: ");
        if (returnValue instanceof Collection) {
            sb.append(((Collection)returnValue).size()).append(" item(s).");
        } else {
            sb.append(returnValue.toString());
        }
        log.info(sb.toString());
    }

    @Around(value = "executeMeasureLogging()")
    public Object measureLogMethodCall(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.nanoTime();
        Object returnValue = joinPoint.proceed();
        long totalTime = System.nanoTime() - startTime;
        StringBuilder sb = new StringBuilder("Method: ");
        sb.append(joinPoint.getSignature().getName());
        sb.append(" totalTime: ").append(totalTime).append("ns");
        Object[] args = joinPoint.getArgs();
        if (null != args && args.length > 0) {
            sb.append(" args= [ | ");
            Arrays.asList(args).forEach(arg -> {sb.append(arg).append(" | ");
            });
            sb.append("]");
        }

        sb.append(", returns: ");
        if (returnValue instanceof Collection) {
            sb.append(((Collection)returnValue).size()).append(" item(s).");
        } else {
            sb.append(returnValue.toString());
        }
        log.info(sb.toString());
        return returnValue;
    }

}
