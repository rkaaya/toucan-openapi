package com.rkaaya.toucanopenapi.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DefaultAutomobilServiceTest {

    @InjectMocks
    private DefaultAutomobilService defaultAutomobilService;

    @Test
    public void automobilServiceTest() {
        defaultAutomobilService.automobilReport();
    }

}